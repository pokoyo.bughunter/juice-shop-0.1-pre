resource "aws_instance" "juice_shop_instance" {
  ami             = data.aws_ami.ubuntu.id
  subnet_id       = aws_subnet.subnet.id
  instance_type   = var.aws_instance_type.small
  key_name        = var.aws_key_name
 # security_groups = [aws_security_group.juice_shop_sg.name]
  user_data       = "${file("juice-shop-script.sh")}"

  connection {
      type        = "ssh"
      host        = self.public_ip
      user        = "ubuntu"
      private_key = file("~/.ssh/id_rsa.pub")
      timeout     = "4m"
   }

  tags = {
    Name = var.server_name
  }
}

resource "aws_key_pair" "deployer" {
  key_name   = "juice_shop_KP"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDKON3gQP46OurpQWuj1iqykBzc5/KbMJQ2qDSEQB2tYBN1P3NeXcGYu8aWbEArVQwy5KaINlUuKhMJ7FhnCNbuSoKHP4nIf3OgZrHQv8CvMLm5mNpNyJjrx2PCMSXD2KYbq+C8I/JP+zonSQ0lkmQLol0SRlos1zPUJtpyZoHEMaur2NfFDbBfo88og4+P+VHjdcKDAntC5VoX0XYVm+xv+c1Ut/p6T28MaJXog8+n90wUYtfdFVcZrHwDjpNoDRiuh2lc5pl9rXvpf2Ev/zUlyoyWBPNkunOamtAWeYmuc9szSDg6HQ5haA4TaH+UwETV5WZVJfL6oPvkm+r8cMZJ xxx@LAPTOP-8CFS7F50"
}

