#!/bin/bash
#Ubuntu 20.04.3 LTS
#Author: Kennedy Sanchez

sudo apt-get update -y 
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"
sudo apt update -y
sudo apt install -y npm
sudo apt install -y docker-ce 
sudo systemctl status docker