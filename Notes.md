# PATH
- ~/.aws/credentials

# AWS Commands
 - aws configure --profile juice-shop

# ENV

`Windows`
- $env:AWS_PROFILE="juice-shop"  

`Linux/Mac`
- export AWS_PROFILE=juice-shop


# TF Commands
 
 - terraform plan -var-file="vars/template.tfvars"
 - terraform apply -var-file="vars/template.tfvars"
 - terraform init -backend-config=access_key="AKIAV54OSKJ6COXKZL6U" -backend-config=secret_key="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" 




# Docker COMMANDS
- docker login -u ksanchez
- docker build -t ksanchez/juice-shop-0.1.1 .
- docker push ksanchez/juice-shop-0.1.1
- docker pull ksanchez/juice-shop:0.1.1
- docker run -d -p 3000:3000 ksanchez/juice-shop:0.1.1
 
# DockerHub
- https://hub.docker.com/repository/docker/ksanchez/juice-shop-0.1.1

# SonarCloud
- https://sonarcloud.io/project/configuration?id=pokoyo.bughunter_juice-shop-0.1-pre

# Snyk
- https://app.snyk.io/org/pokoyo.bughunter/project/5a79a645-409d-4d96-a913-a470a024f94a



 # Docker Installation

 - https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04-es
 - https://www.simplilearn.com/tutorials/docker-tutorial/how-to-install-docker-on-ubuntu?source=sl_frs_nav_playlist_video_clicked


 # Video Solutions Owasp Juice Shop
- https://www.youtube.com/watch?v=3TKm5T0ul5Y