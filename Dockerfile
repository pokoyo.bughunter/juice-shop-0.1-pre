# Security Issue. Pentesting scenario.
# node:8.12.0-alpine, node:14.1-alpine, node:16.14.2-alpine, nginx:1.21.6  [CVE-2022-0778, CVE-2022-23308]

FROM        nginx:1.21.6-alpine                                 
LABEL       author="Kennedy Sanchez"
#ENV         NODE_ENV=production 
ENV         PORT=3000
WORKDIR     /var/www
COPY        package.json ./
COPY        . ./
#RUN         npm install --production=false
#RUN           npm install --production=false && npm audit --registry=https://registry.npmjs.org fix
EXPOSE      $PORT
ENTRYPOINT  ["npm", "start"]
