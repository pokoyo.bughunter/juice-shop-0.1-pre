#############
# PROVIDERS #
#############

provider "aws" {
  region         = var.aws_region
  access_key     = var.aws_access_key
  secret_key     = var.aws_secret_key
}

###############
#  RESOURCES  #
###############

resource "aws_vpc" "vpc" {
  cidr_block           = var.aws_vpc_cidr_block
  enable_dns_hostnames = var.enable_dns_hostnames

  tags = {
    Name          = var.aws_vpc_name
    Environment   = "Vulnerable Development Environment"
    Author        = "Kennedy Sanchez"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "Juice Shop Main IGW"
  }
}

resource "aws_subnet" "subnet" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.private_subnet[0]
  map_public_ip_on_launch = var.map_public_ip_on_launch
  availability_zone       = "us-east-1a"
}

resource "aws_route_table" "rtb" {
  vpc_id = aws_vpc.vpc.id
  
  route {
    cidr_block = "0.0.0.0/0"                                          # Security Issue. Pentesting scenario.
    gateway_id = aws_internet_gateway.igw.id
  }
    tags = {
    Name = "Juice Shop Route Table"
  }
}

resource "aws_route_table_association" "rta_subnet" {
  subnet_id      = aws_subnet.subnet.id
  route_table_id = aws_route_table.rtb.id
}

#################
#  DATA SOURCE  #
#################

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}