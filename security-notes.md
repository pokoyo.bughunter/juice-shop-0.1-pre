# Amazon Infrastructure
1. Acces Keys Hardcoded.
2. No availability zones defined.
3. No load balancer defined.
4. Just one region defined.
5. Security Group Allows All Networks.

# Docker
1. Vulnerable Docker Image 
     - https://snyk.io/test/docker/node%3A14.1-alpine
     - https://snyk.io/test/docker/node%3A8.12.0-alpine
     - https://github.com/nodejs/node/issues/42441


# Vulnerability Scanners
1. SNYK             :  docker scan --token 44206321-be58... juice-shop-0.1.1
2. npm audit