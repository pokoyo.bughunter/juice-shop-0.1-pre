resource "aws_security_group" "juice_shop_sg" {
  #name         = var.aws_sg_name
  name          = "juice_shop_SG"
  description   = "Allow Traffic"
  vpc_id        = aws_vpc.vpc.id

  ingress {
    description      = "Allow from Personal CIDR block"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = [var.aws_vpc_cidr_block]
    # cidr_blocks      = [var.cidr_block]
  }

  ingress {
    description      = "Allow SSH from Personal CIDR block"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]                       # Security Issue. Pentesting scenario.
    #cidr_blocks      = [var.cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]                       # Security Issue. Pentesting scenario.
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "Juice-Shop SG"
  }
}

resource "aws_security_group" "juice_shop_alb_sg" {
  #name        = var.aws_sg_name
  name = "juice_shop_alb_SG"
  description = "Allow Traffic"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    description      = "Allow from Personal CIDR block"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]                            # Security Issue. Pentesting scenario.
    # cidr_blocks      = [var.cidr_block]
  }

  ingress {
    description      = "Allow SSH from Personal CIDR block"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]                             # Security Issue. Pentesting scenario.
    #cidr_blocks      = [var.cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]                              # Security Issue. Pentesting scenario.
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "Juice-Shop SG"
  }
}