#############
# VARIABLES #
#############
variable "aws_access_key" {
  type = string
  default        = "AKIAV54OSKJ6COXKZL6U"                          # Security Issue. Pentesting scenario.
  description    = "The AWS access key."
  sensitive      = true
}

variable "aws_secret_key" {
  type = string
  default        = "yVlTCPUl7FsMPFzZc5kDiB3KTbOs84ZFTdIPMqvH"       # Security Issue. Pentesting scenario.
  description    = "The AWS secret key."
  sensitive      = true
}

variable "aws_vpc_name" {
  type          = string
  default       = "Juice_Shop_VPC_Dev"
  description   = "VPC Name"
}

variable "aws_region" {
  type          = string
  default       = "us-east-1"
  description   = "AWS East region to create resources"
}

variable "aws_vpc_cidr_block" {
  type          = string
  default       = "10.10.0.0/16"
  description   = "CIDR Block Main VPC East Region"
}

variable "public_subnet" {
  type = list(string)
  default       = ["10.10.0.0/24", "10.10.1.0/24"]
  description   = "CIDR Block Subnet East Region"
}

variable "private_subnet" {
  type = list(string)
  default       = ["10.10.5.0/24", "10.10.6.0/24"]
  description   = "CIDR Block Subnet East Region"
}

variable "aws_instance_type" {
 # type = string
  default = {
    small   = "t2.micro",
    medium  = "t2.small"
  }
}

variable "aws_key_name" {
  type = string
  default = "juice_shop_KP"
  description = "SSH key name in your AWS account for AWS instances."
}

variable "company" {
  type = string
  description = "SappoTech a great Security Company by Ksanchez"
  default = "SappoTech"
}

variable "project" {
  type = string
  default = "Juice Shop"
}

variable "server_name" {
  type = string
  description = "The name of the server."
  default = "Juice_Shop_Server"
}

variable "aws_s3_bucket" {
  type = string
  default = "Juice_Shop_S3"
  description = "S3 bucket where remote state and data will be stored."
}

variable "aws_sg_name" {
  type = string
  default = "Juice_Shop_SG"
  description = "Security Group Name"
}

variable "map_public_ip_on_launch" {
  type = bool
  default = true
  description = "Mapping instances to Public IP Address."
}

variable "enable_dns_hostnames" {
  type  = bool
  default = true
  description = "Enable DNS hostname in VPC"
}