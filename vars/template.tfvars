aws_region     = "us-east-1"
vpc_id         = var.aws_vpc_name
cidr_block     = var.aws_vpc_cidr_block
key_name       = var.aws_key_name